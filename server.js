var {google} = require('googleapis');
var googleAuth = require('google-auth-library');
var fs = require('fs');
var readline = require('readline');
var http = require("http");
var https = require('https');
var scheduler = require('node-schedule');
var config = require('./config.json');
const url = require('url');

var SCOPES = ['https://www.googleapis.com/auth/admin.directory.user.readonly'];
var TOKEN_PATH = 'auth.json';

var cachedResults = [];
var results = [];

var oauth2Client;

function getPagedUsers(auth, nextPageToken, callback){
    google.admin('directory_v1').users.list({auth: auth, domain: config.domain, maxResults: 500, pageToken: nextPageToken}, function(err, response) {
            if (err) {
                console.log('The API returned an error: ' + err);
                callback(err);
                return;
            }

            response.data.users.forEach(function(value) {
                user = {cn: value.name.fullName, mail: value.primaryEmail};
                if (config.exclusions.indexOf(user.mail) >= 0) return;

                if (value.phones) {
                    mobile = value.phones.find(function (element) { return element.type === 'mobile'; });
                    work = value.phones.find(function (element) { return element.type === 'work'; });

                    if (mobile != undefined) {
                        user["mobile"] = getPhoneWithPrefix(mobile.value);
                    }
                    if (work != undefined) {
                        user["telephoneNumber"] = getPhoneWithPrefix(work.value);
                    }
                }
                results.push(user);
            });
            if (response.data.nextPageToken){
                getPagedUsers(auth,response.data.nextPageToken, callback);
            }
            else{
                callback(undefined, results);
            }
        })
}

function getPhoneWithPrefix(number) {
    if (number && config.mobile_prefix ) {
        if (number.substring(0,2) == "00" || number.substring(0,1) == "+") {
            return number;
        }
        if (number.substring(0,1) == "0") {
            number = config.mobile_prefix + number.substring(1);
        } else {
            number = config.mobile_prefix + number;
        }
    }
    return number;
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    var redirectUrl = config.redirectURI;
    var auth = new googleAuth.GoogleAuth();
    oauth2Client = new googleAuth.OAuth2Client(clientId, clientSecret, redirectUrl);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function(err, token) {
        if (err) {
            getNewToken(oauth2Client, callback);
        } else {
            oauth2Client.credentials = JSON.parse(token);
            callback(oauth2Client);
        }
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
    console.log('Writing token');

    fs.writeFileSync(TOKEN_PATH, JSON.stringify(token));
    fetchData();
}

function runserver(request, response) {
    if (config.redirectURI && config.api_port && config.redirectURI.split(config.api_port).length > 1){
        var redirectURIreference = config.redirectURI.split(config.api_port)[1];
        if (request.url.includes(redirectURIreference)){
            var url = new URL(config.redirectURI.split(config.api_port)[0] + request.url);
            var code = url.searchParams.get('code')
            oauth2Client.getToken(code, function(err, token) {
                if (err) {
                    console.log('Error while trying to retrieve access token', err);
                    return;
                }
                oauth2Client.credentials = token;
                storeToken(token);
                console.log('Waiting for requests...');
            });
            response.writeHead(200, {"Content-Type": "application/json; charset=utf-8", "Access-Control-Allow-Origin" : "*", "Access-Control-Allow-Headers" :  "X-Requested-With, Content-Type, Authorization, Language"});
            response.write("Vizito integration authenticated. You may close this window.");
            response.end();
        }
        else{
            if (cachedResults.length > 0){
                response.writeHead(200, {"Content-Type": "application/json; charset=utf-8", "Access-Control-Allow-Origin" : "*", "Access-Control-Allow-Headers" :  "X-Requested-With, Content-Type, Authorization, Language"});
    
                response.write(cachedResults);
                response.end();
            }
        }
    }
    else{
        response.writeHead(200, {"Content-Type": "application/json; charset=utf-8", "Access-Control-Allow-Origin" : "*", "Access-Control-Allow-Headers" :  "X-Requested-With, Content-Type, Authorization, Language"});

        response.write("Missing configuration entries");
        response.end();
    }
};

var hourlyRule = new scheduler.RecurrenceRule();
hourlyRule.minute = 0;

scheduler.scheduleJob(hourlyRule, fetchData);

function fetchData() {
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            console.log('Error loading client secret file: ' + err);
            return;
        }

        authorize(JSON.parse(content), function (auth)  {
            results = [];
            getPagedUsers(auth, undefined, function (err, result) {
                if (err) {
                    console.log('The API returned an error: ' + err);
                    return;
                }
                cachedResults = JSON.stringify(result);
            })
        });

    });
};

fetchData();

var server;

if (config.use_ssl_for_api) {
    var caArray = [];
    if (config.ssl.ca && config.ssl.ca.length > 0) {
        config.ssl.ca.forEach(function (caReference) {
            caArray.push(fs.readFileSync(caReference));
        });
    }

    server = https.createServer({
        ca: caArray,
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.cert),
        ciphers: [
            "ECDHE-RSA-AES256-SHA384",
            "DHE-RSA-AES256-SHA384",
            "ECDHE-RSA-AES256-SHA256",
            "DHE-RSA-AES256-SHA256",
            "ECDHE-RSA-AES128-SHA256",
            "DHE-RSA-AES128-SHA256",
            "HIGH",
            "!aNULL",
            "!eNULL",
            "!EXPORT",
            "!DES",
            "!RC4",
            "!MD5",
            "!PSK",
            "!SRP",
            "!CAMELLIA"
        ].join(':')
    }, function(request, response) {
        runserver(request, response);
    });
} else {
    server = http.createServer(function(request, response) {
        runserver(request, response);
    });
}

server.listen(config.api_port);

console.log((config.use_ssl_for_api ? "HTTPS" : "HTTP") + " server is listening on port " + config.api_port);
